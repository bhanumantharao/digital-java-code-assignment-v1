package com.singtel.digital.assignment.repository;

import com.singtel.digital.assignment.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Hanumantharao Bitragunta
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
}
