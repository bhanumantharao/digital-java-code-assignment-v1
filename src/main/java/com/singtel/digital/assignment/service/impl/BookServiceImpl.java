package com.singtel.digital.assignment.service.impl;

import com.singtel.digital.assignment.exception.BookNotFoundException;
import com.singtel.digital.assignment.model.Book;
import com.singtel.digital.assignment.repository.BookRepository;
import com.singtel.digital.assignment.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Hanumantharao Bitragunta
 */
@Service
public class BookServiceImpl implements BookService {

    private Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    BookRepository bookRepository;

    @Override
    public List<Book> addBook(Book book){
        bookRepository.save(book);

        return getAllBooks();
    }

    @Override
    public Book getBook(int bookId){
        // java8
        Optional<Book> book = bookRepository.findById(bookId);
        if(!book.isPresent())
            throw new BookNotFoundException("The given book id not found: "+bookId+" Please try again with valid Id.");

        return book.get();
    }

    private List<Book> getAllBooks(){ return bookRepository.findAll();}


}
