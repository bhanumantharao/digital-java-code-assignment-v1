package com.singtel.digital.assignment.service;

import com.singtel.digital.assignment.model.Book;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Hanumantharao Bitragunta
 */
@Component
public interface BookService {

    Book getBook(int bookId);
    List<Book> addBook(Book book);
}
