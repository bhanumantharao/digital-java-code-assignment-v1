package com.singtel.digital.assignment.controller;

import com.singtel.digital.assignment.model.Book;
import com.singtel.digital.assignment.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Hanumantharao Bitragunta
 */

@RestController
@RequestMapping("/api")
public class BookController {

    private static Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    BookService bookService;

    @GetMapping(value = "/books/{bookId}")
    public ResponseEntity<Book> getBook(@PathVariable int bookId) {
        logger.info("##########  - Get Book - #########");
        Book book = bookService.getBook(bookId);
        return new ResponseEntity(book, HttpStatus.OK);
    }

    @PostMapping(value = "/book")
    public ResponseEntity addBook(@Valid @RequestBody Book book){
        logger.info("##########  - Add Book - #########");
        List<Book> booksList = bookService.addBook(book);

        return new ResponseEntity(booksList, HttpStatus.OK);
    }
}
