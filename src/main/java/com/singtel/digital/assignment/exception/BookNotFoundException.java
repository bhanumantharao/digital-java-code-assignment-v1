package com.singtel.digital.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hanumantharao Bitragunta
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(String exception) {super(exception);}
}
