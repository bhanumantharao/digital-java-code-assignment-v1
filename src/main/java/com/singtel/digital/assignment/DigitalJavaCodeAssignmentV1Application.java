package com.singtel.digital.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class DigitalJavaCodeAssignmentV1Application {

	public static void main(String[] args) {
		SpringApplication.run(DigitalJavaCodeAssignmentV1Application.class, args);
	}

}
