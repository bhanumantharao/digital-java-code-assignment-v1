package com.singtel.digital.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Hanumantharao Bitragunta
 */

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="BOOK")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    @JsonProperty("ID")
    int id;

    @Column(name = "BOOK_ID")
    @JsonProperty("BookId")
    @NotNull(message = "Please provide Book Id")
    String bookId;

    @Column(name = "BOOK_NAME")
    @JsonProperty("BookName")
    @NotNull(message = "Please provide Book Name")
    String bookName;

    @Column(name = "TYPE")
    @JsonProperty("Type")
    @NotNull(message = "Please provide Book type")
    String type;  // Function || Non-Function

    @Column(name = "GENRES")
    @JsonProperty("Genres")
    @NotNull(message = "Please provide genres")
    String genres; // Function(Action and adventure, Anthology, Crime .. etc) || Non-Function (Art, Auto-Biography.. ect)

    @Column(name = "AUTHOR_ID")
    @JsonProperty("AuthorId")
    @NotNull(message = "Please provide Book Author Id")
    int authorId;
}
