package com.singtel.digital.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.singtel.digital.assignment.DigitalJavaCodeAssignmentV1ApplicationTests;
import com.singtel.digital.assignment.model.Book;
import com.singtel.digital.assignment.service.BookService;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author Hanumantharao Bitragunta
 */

@Ignore
public class BookControllerTest extends DigitalJavaCodeAssignmentV1ApplicationTests {

    @Autowired private MockMvc mockMvc;
    @Autowired private ObjectMapper mapper;

    @MockBean
    BookService bookService;

    @Test
    public void getBook() throws Exception {
        List<Book> list = new ArrayList<>();
        list.add(getBookObj());

        when(bookService.addBook(getBookObj())).thenReturn(list);

        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/api/book")
                .content(asJsonString(getBookObj()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
    }

    @Test
    public void addBook() throws Exception {

        when(bookService.getBook(1111)).thenReturn(getBookObj());

        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/api/books/21")
                .requestAttr("bookId", 1111)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

    }

    private Book getBookObj(){
        return new Book(1111, "bk_001", "The Panlo", "Novel", "Fiction", 4);
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}